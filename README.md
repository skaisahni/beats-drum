# Drum Beats application  using React, Redux, Javascript ,css , html #

*  drum beats application which plays all the 9 beats of drum set on the button click.
*  Technology: Html, CSS, Javascript, Redux ,react
* Version:1:0

# Files :

* Index.html for html body;
* style.css  for styling of pages;
* logicvisual.js for drum logic to make sound of drum on button's click;



